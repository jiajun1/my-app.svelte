const loginURL = "http://localhost:5001/api/v1.0/login"
const registerURL = "http://localhost:5001/api/v1.0/register"
const resetURL = "http://localhost:5001/api/v1.0/reset_password/<string:username>"
const generateTokenURL = "http://localhost:5001/api/v1.0/generate_token/<string:username>"
const confirmTokenURL = "http://localhost:5001/api/v1.0/confirm_token"
const logoutURL = "http://localhost:5001/api/v1.0/logout"
const updatePasswordURL = "http://localhost:5001/api/v1.0/update/<string:username>"
const getUsernameURL = "http://localhost:5001/api/v1.0/profile/get_username"

export {loginURL}
export {registerURL}
export {resetURL}
export {generateTokenURL}
export {confirmTokenURL}
export {logoutURL}
export {updatePasswordURL}
export {getUsernameURL}
