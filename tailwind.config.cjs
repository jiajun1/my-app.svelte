const config = {
  content: ["./src/**/*.{html,js,cjs,svelte,ts}"],

  theme: {
    backgroundImage: {
      'background': "url('../static/background')",
    },
  },

  plugins: [],
};

module.exports = config;
